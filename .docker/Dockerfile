# Base -------------------------------------------------------------------------

FROM phpearth/php:7.3-apache AS base
RUN apk add --no-cache \
        php7.3-intl \
        php7.3-pdo_pgsql \
        php7.3-redis
COPY .docker/vhost.conf /etc/apache2/conf.d/vhost.conf
COPY .docker/php.ini /etc/php/7.3/conf.d/99_php.ini
WORKDIR /workspace

# Development ------------------------------------------------------------------

FROM base AS development
RUN apk add --no-cache \
        composer \
        shadow \
        yarn && \
    mkdir -p /var/www/.composer /var/www/.cache/yarn && \
    chown -R apache:apache /var/www
COPY .docker/entrypoint.sh /sbin/entrypoint
RUN chmod +x /sbin/entrypoint
ENTRYPOINT ["/sbin/entrypoint"]
CMD ["/sbin/runit-wrapper"]

# Production -------------------------------------------------------------------

FROM development AS build
COPY . /workspace
RUN composer install --no-scripts --optimize-autoloader --no-suggest --no-dev && \
    composer dump-env prod --empty && \
    yarn install && \
    yarn encore production && \
    rm -rf .docker/ .git/ assets/ node_modules/ tests/ var/

FROM base AS production
COPY --from=build /workspace /workspace
RUN chown apache:apache -R /workspace
